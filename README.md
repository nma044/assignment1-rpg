## RPG Heroes
RPG Heroes is a setup for a simple text-based rpg game. It contains 4 different classes,
all with different stats, and different weapons and armor they can equip.

## Description
The four different classes are Mage, Ranger, Rogue and Warrior. They each have 3 different
attributes: Intelligence, Dexterity and Strength. The classes all start with different
values for these attributes, and gain different amounts for each attribute
when they level up. There exists armor they can equip, which have attributes the heroes
gain while wearing the armor. There are also weapons which increase the heroes damage.

## Dependencies
The program is made using basic Java, and the tests are made using the JUnit framework.

## Usage
To create a new Mage you have to write: 
Mage mage = new Mage("username");
And the same for all the other classes.
to level up your hero you use the levelUp() method in the Hero class:
mage.levelup();
to print out the stats of your hero you can use the display() method:
mage.display()
to equip a new weapon or armor for your hero you can use the equip() method:
mage.equip(weapon) and mage.equip(armor)
For this to work you first have to create your weapon and armor object


