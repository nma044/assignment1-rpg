package Heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {


    @Test
    public void testCreateRogue_name(){
        //Arrange
        String expected = "user";
        //Act
        Rogue test = new Rogue("user");
        String actual = test.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateRogue_attributes(){
        //Arrange
        HeroAttribute expected = new HeroAttribute(2,6,1);
        //Act
        Rogue test = new Rogue("");
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void testLevelUpRogue_attributes(){
        //Arrange
        Rogue test = new Rogue("");
        HeroAttribute expected = new HeroAttribute(3,10,2);
        //Act
        test.levelUp();
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testLevelUpRogue_level(){
        //Arrange
        Rogue test = new Rogue("");
        int expected = 2;
        //Act
        test.levelUp();
        int actual = test.getLevel();
        //Assert
        assertEquals(expected, actual);
    }


}