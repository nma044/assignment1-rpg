package Heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    public void testCreateWarrior_name(){
        //Arrange
        String expected = "user";
        //Act
        Warrior test = new Warrior("user");
        String actual = test.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateWarrior_attributes(){
        //Arrange
        HeroAttribute expected = new HeroAttribute(5,2,1);
        //Act
        Warrior test = new Warrior("");
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void testLevelUpWarrior_attributes(){
        //Arrange
        Warrior test = new Warrior("");
        HeroAttribute expected = new HeroAttribute(8,4,2);
        //Act
        test.levelUp();
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testLevelUpWarrior_level(){
        //Arrange
        Warrior test = new Warrior("");
        int expected = 2;
        //Act
        test.levelUp();
        int actual = test.getLevel();
        //Assert
        assertEquals(expected, actual);
    }


}