package Heroes;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.ArmorAttribute;
import Items.Item;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    public void testEquipWeapon_shouldPass() throws InvalidWeaponException {
        //Arrange
        Mage test = new Mage("");
        Weapon expected = new Weapon("test",1,1, Weapon.WeaponType.STAFF);
        //Act
        test.equip(expected);
        Item actual = test.getEquipment().get(Item.Slot.WEAPON);
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void testEquipWeapon_shouldThrowException_tooLowLevel(){
        //Arrange
        Mage test = new Mage("");
        Weapon weapon = new Weapon("test",2,1, Weapon.WeaponType.STAFF);
        //Act
        InvalidWeaponException thrown = assertThrows(InvalidWeaponException.class,() -> {
            test.equip(weapon);
        });
        //Assert
        assertEquals("Too low level", thrown.getMessage());
    }

    @Test
    public void testEquipWeapon_shouldThrowException_invalidWeaponType(){
        //Arrange
        Mage test = new Mage("");
        Weapon weapon = new Weapon("test",1,1, Weapon.WeaponType.AXE);
        //Act
        InvalidWeaponException thrown = assertThrows(InvalidWeaponException.class,() -> {
            test.equip(weapon);
        });
        //Assert
        assertEquals("Invalid weapon type", thrown.getMessage());
    }

    @Test
    public void testEquipArmor_shouldPass() throws InvalidArmorException {
        //Arrange
        Mage test = new Mage("");
        Armor expected = new Armor("test", 1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        //Act
        test.equip(expected);
        Item actual = test.getEquipment().get(Item.Slot.BODY);
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void testEquipArmor_shouldThrowException_tooLowLevel() {
        //Arrange
        Mage test = new Mage("");
        Armor armor = new Armor("test", 2, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        //Act
        InvalidArmorException thrown = assertThrows(InvalidArmorException.class,() -> {
            test.equip(armor);
        });
        //Assert
        assertEquals("Too low level", thrown.getMessage());
    }

    @Test
    public void testEquipArmor_shouldThrowException_invalidWeaponType(){
        //Arrange
        Mage test = new Mage("");
        Armor armor = new Armor("test", 1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.PLATE);
        //Act
        InvalidArmorException thrown = assertThrows(InvalidArmorException.class,() -> {
            test.equip(armor);
        });
        //Assert
        assertEquals("Invalid armor type", thrown.getMessage());
    }

    @Test
    public void testTotalAttributes_noEquipment(){
        //Arrange
        Mage test = new Mage("");
        HeroAttribute expected = new HeroAttribute(1,1,8);
        //Act
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testTotalAttributes_oneArmor() throws InvalidArmorException {
        //Arrange
        Mage test = new Mage("");
        Armor armor = new Armor("armor", 1, Item.Slot.HEAD, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        test.equip(armor);
        HeroAttribute expected = new HeroAttribute(2,2,9);
        //Act
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testTotalAttributes_twoArmor() throws InvalidArmorException {
        //Arrange
        Mage test = new Mage("");
        Armor armor = new Armor("armor", 1, Item.Slot.HEAD, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        Armor armor2 = new Armor("armor2", 1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        test.equip(armor);
        test.equip(armor2);
        HeroAttribute expected = new HeroAttribute(3,3,10);
        //Act
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testTotalAttributes_replaceOneArmor() throws InvalidArmorException {
        //Arrange
        Mage test = new Mage("");
        Armor armor = new Armor("armor", 1, Item.Slot.HEAD, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        Armor armor2 = new Armor("armor", 1, Item.Slot.HEAD, new ArmorAttribute(0,0,1), Armor.ArmorType.CLOTH);
        test.equip(armor);
        test.equip(armor2);
        HeroAttribute expected = new HeroAttribute(1,1,9);
        //Act
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDamage_noWeapon(){
        //Arrange
        Mage test = new Mage("");
        double expected = 1.0 + (8.0/100.0);
        //Act
        double actual = test.damage();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDamage_oneWeapon() throws InvalidWeaponException {
        //Arrange
        Mage test = new Mage("");
        double expected = 2.0 *(1.0+ 8.0/100.0);
        Weapon weapon = new Weapon("weapon", 1,2, Weapon.WeaponType.STAFF);
        //Act
        test.equip(weapon);
        double actual = test.damage();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDamage_oneWeaponReplaced() throws InvalidWeaponException {
        //Arrange
        Mage test = new Mage("");
        double expected = 3.0 *(1.0+ 8.0/100.0);
        Weapon weapon = new Weapon("weapon", 1,2, Weapon.WeaponType.STAFF);
        Weapon weapon2 = new Weapon("weapon2", 1,3, Weapon.WeaponType.STAFF);

        //Act
        test.equip(weapon);
        test.equip(weapon2);
        double actual = test.damage();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDamage_oneWeaponOneArmor() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Mage test = new Mage("");
        double expected = 2.0 *(1.0+ ((8.0+1.0)/100.0));
        Armor armor = new Armor("armor", 1, Item.Slot.HEAD, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        Weapon weapon = new Weapon("weapon", 1,2, Weapon.WeaponType.STAFF);
        //Act
        test.equip(armor);
        test.equip(weapon);
        double actual = test.damage();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDisplay(){
        //Arrange
        Mage test = new Mage("test");
        StringBuilder expectedString = new StringBuilder();

        expectedString.append("Name: "+ "test" + '\n');
        expectedString.append("Class: "+ "Mage"+'\n');
        expectedString.append("Level: "+1+'\n');
        expectedString.append("Total strength: " + 1+'\n');
        expectedString.append("Total dexterity: " + 1+'\n');
        expectedString.append("Total intelligence: " + 8+'\n');
        expectedString.append("Damage: "+1.08);

        String expected = expectedString.toString();
        //Act
        String actual = test.display();
        //Assert
        assertEquals(expected,actual);
    }

}