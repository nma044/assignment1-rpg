package Heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    @Test
    public void testCreateRanger_name(){
        //Arrange
        String expected = "user";
        //Act
        Ranger test = new Ranger("user");
        String actual = test.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateRanger_attributes(){
        //Arrange
        HeroAttribute expected = new HeroAttribute(1,7,1);
        //Act
        Ranger test = new Ranger("");
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void testLevelUpRanger_attributes(){
        //Arrange
        Ranger test = new Ranger("");
        HeroAttribute expected = new HeroAttribute(2,12,2);
        //Act
        test.levelUp();
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testLevelUpRanger_level(){
        //Arrange
        Ranger test = new Ranger("");
        int expected = 2;
        //Act
        test.levelUp();
        int actual = test.getLevel();
        //Assert
        assertEquals(expected, actual);
    }


}