package Heroes;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.ArmorAttribute;
import Items.Item;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    public void testCreateMage_name(){
        //Arrange
        String expected = "user";
        //Act
        Mage test = new Mage("user");
        String actual = test.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateMage_attributes(){
        //Arrange
        HeroAttribute expected = new HeroAttribute(1,1,8);
        //Act
        Mage test = new Mage("");
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void testLevelUpMage_attributes(){
        //Arrange
        Mage test = new Mage("");
        HeroAttribute expected = new HeroAttribute(2,2,13);
        //Act
        test.levelUp();
        HeroAttribute actual = test.totalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testLevelUpMage_level(){
        //Arrange
        Mage test = new Mage("");
        int expected = 2;
        //Act
        test.levelUp();
        int actual = test.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

}