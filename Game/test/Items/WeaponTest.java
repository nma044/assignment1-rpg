package Items;

import Heroes.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    public void testCreateWeapon_name(){
        //Arrange
        String expected = "Weapon";
        //Act
        Weapon test = new Weapon("Weapon",1,1, Weapon.WeaponType.AXE);
        String actual = test.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateWeapon_requiredLevel(){
        //Arrange
        int expected = 1;
        //Act
        Weapon test = new Weapon("Weapon",1,1, Weapon.WeaponType.AXE);
        int actual = test.getRequiredLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateWeapon_slot(){
        //Arrange
        Item.Slot expected = Item.Slot.WEAPON;
        //Act
        Weapon test = new Weapon("Weapon",1,1, Weapon.WeaponType.AXE);
        Item.Slot actual = test.getSlot();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateWeapon_weaponType(){
        //Arrange
        Weapon.WeaponType expected = Weapon.WeaponType.AXE;
        //Act
        Weapon test = new Weapon("Weapon",1,1, Weapon.WeaponType.AXE);
        Weapon.WeaponType actual = test.getWeaponType();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateWeapon_damage(){
        //Arrange
        int expected = 1;
        //Act
        Weapon test = new Weapon("Weapon",1,1, Weapon.WeaponType.AXE);
        int actual = test.getWeaponDamage();
        //Assert
        assertEquals(expected, actual);
    }

}