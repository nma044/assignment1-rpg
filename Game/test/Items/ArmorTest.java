package Items;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    public void testCreateArmor_name(){
        //Arrange
        String expected = "Armor";
        //Act
        Armor test = new Armor("Armor",1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        String actual = test.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateArmor_requiredLevel(){
        //Arrange
        int expected = 1;
        //Act
        Armor test = new Armor("Armor",1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        int actual = test.getRequiredLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateArmor_slot(){
        //Arrange
        Item.Slot expected = Item.Slot.BODY;
        //Act
        Armor test = new Armor("Armor",1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        Item.Slot actual = test.getSlot();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateArmor_armorAttributes(){
        //Arrange
        ArmorAttribute expected = new ArmorAttribute(1,1,1);
        //Act
        Armor test = new Armor("Armor",1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        ArmorAttribute actual = test.getAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateArmor_damage(){
        //Arrange
        Armor.ArmorType expected = Armor.ArmorType.CLOTH;
        //Act
        Armor test = new Armor("Armor",1, Item.Slot.BODY, new ArmorAttribute(1,1,1), Armor.ArmorType.CLOTH);
        Armor.ArmorType actual = test.getArmorType();
        //Assert
        assertEquals(expected, actual);
    }

}