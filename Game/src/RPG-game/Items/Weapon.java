package Items;

public class Weapon extends Item{
    public enum WeaponType{
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    private int weaponDamage;
    private WeaponType weaponType;

    public Weapon(String name, int requiredLevel, int weaponDamage, WeaponType weaponType) {
        super(name, requiredLevel, Item.Slot.WEAPON);
        this.weaponDamage = weaponDamage;
        this.weaponType = weaponType;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
}
