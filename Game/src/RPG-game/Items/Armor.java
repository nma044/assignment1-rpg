package Items;

public class Armor extends Item{
    public enum ArmorType{
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    private ArmorAttribute attributes;
    private ArmorType armorType;
    public Armor(String name, int requiredLevel, Slot slot, ArmorAttribute attributes, ArmorType armortype) {
        super(name, requiredLevel, slot);
        this.attributes = attributes;
        this.armorType = armortype;
    }

    public ArmorAttribute getAttributes() {
        return attributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

}
