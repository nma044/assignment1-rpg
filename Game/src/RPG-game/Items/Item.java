package Items;

public abstract class Item {

    public enum Slot {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }

    public String getName() {
        return Name;
    }

    protected String Name;

    public int getRequiredLevel() {
        return RequiredLevel;
    }

    protected int RequiredLevel;

    public Slot getSlot() {
        return slot;
    }

    protected Slot slot;

    public Item(String name, int requiredLevel, Slot slot) {
        Name = name;
        RequiredLevel = requiredLevel;
        this.slot = slot;
    }


}
