package Exceptions;

public class InvalidWeaponException extends Exception{

    public InvalidWeaponException(){}

    public InvalidWeaponException(String message){
        super(message);
    }
}
