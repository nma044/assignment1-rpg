package Exceptions;

public class InvalidArmorException extends Exception{

    public InvalidArmorException(){}

    public InvalidArmorException(String message){
        super(message);
    }
}