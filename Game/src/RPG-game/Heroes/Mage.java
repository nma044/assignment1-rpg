package Heroes;

import Items.Armor;
import Items.Item;
import Items.Weapon;

public class Mage extends Hero{

    public Mage(String name) {
        super(name);
        attributes = new HeroAttribute(1,1,8);
        validWeaponTypes.add(Weapon.WeaponType.STAFF);
        validWeaponTypes.add(Weapon.WeaponType.WAND);
        validArmorTypes.add(Armor.ArmorType.CLOTH);
    }

    @Override
    public double damage() {
        double damage = 1.0 + (totalAttributes().getIntelligence())/100.0;
        if(equipment.get(Item.Slot.WEAPON) == (null)) return damage;
        return ((Weapon)equipment.get(Item.Slot.WEAPON)).getWeaponDamage() * damage;
    }

    @Override
    public void levelUp() {
        super.levelUp();
        attributes.addIntelligence(5);
        attributes.addDexterity(1);
        attributes.addStrength(1);
    }
}
