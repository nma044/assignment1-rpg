package Heroes;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Weapon;

public interface IHero {

    public void levelUp();

    public void equip(Weapon weapon) throws InvalidWeaponException;

    public void equip(Armor armor) throws InvalidArmorException;

    public double damage();

    public HeroAttribute totalAttributes();

    public String display();

}
