package Heroes;

import Items.Armor;
import Items.Item;
import Items.Weapon;

public class Rogue extends Hero{

    public Rogue(String name) {
        super(name);
        attributes = new HeroAttribute(2,6,1);
        validWeaponTypes.add(Weapon.WeaponType.DAGGER);
        validWeaponTypes.add(Weapon.WeaponType.SWORD);
        validArmorTypes.add(Armor.ArmorType.LEATHER);
        validArmorTypes.add(Armor.ArmorType.MAIL);
    }

    @Override
    public double damage() {
        double damage =1.0 + (totalAttributes().getDexterity())/100.0;
        if(equipment.get(Item.Slot.WEAPON) == (null)) return damage;
        return ((Weapon)equipment.get(Item.Slot.WEAPON)).getWeaponDamage() * damage;
    }

    @Override
    public void levelUp() {
        super.levelUp();
        attributes.addIntelligence(1);
        attributes.addDexterity(4);
        attributes.addStrength(1);
    }

}
