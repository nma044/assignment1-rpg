package Heroes;

import Items.Armor;
import Items.Item;
import Items.Weapon;

public class Ranger extends Hero {

    public Ranger(String name) {
        super(name);
        attributes = new HeroAttribute(1,7,1);
        validWeaponTypes.add(Weapon.WeaponType.BOW);
        validArmorTypes.add(Armor.ArmorType.LEATHER);
        validArmorTypes.add(Armor.ArmorType.MAIL);
    }

    @Override
    public double damage() {
        double damage = 1.0 + (totalAttributes().getDexterity())/100.0;
        if(equipment.get(Item.Slot.WEAPON) == (null)) return damage;
        return ((Weapon)equipment.get(Item.Slot.WEAPON)).getWeaponDamage() * damage;
    }

    @Override
    public void levelUp() {
        super.levelUp();
        attributes.addIntelligence(1);
        attributes.addDexterity(5);
        attributes.addStrength(1);
    }

}
