package Heroes;

public class HeroAttribute {
    private int Strength;
    private int Dexterity;
    private int Intelligence;

    public HeroAttribute(int s, int d, int i){
        Strength = s;
        Dexterity = d;
        Intelligence = i;
    }



    public int getStrength() {
        return Strength;
    }

    public void addStrength(int strength) {
        Strength+= strength;
    }

    public int getDexterity() {
        return Dexterity;
    }

    public void addDexterity(int dexterity) {
        Dexterity+= dexterity;
    }

    public int getIntelligence() {
        return Intelligence;
    }

    public void addIntelligence(int intelligence) {
        Intelligence+= intelligence;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if (!(obj instanceof HeroAttribute)) return false;
        HeroAttribute o = (HeroAttribute) obj;
        if(o.getIntelligence() == getIntelligence() && o.getStrength() == getStrength() && o.getDexterity() == getDexterity()) return true;
        return false;}

}
