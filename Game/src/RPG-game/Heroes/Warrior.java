package Heroes;

import Items.Armor;
import Items.Item;
import Items.Weapon;

public class Warrior extends Hero{

    public Warrior(String name) {
        super(name);
        attributes = new HeroAttribute(5,2,1);
        validWeaponTypes.add(Weapon.WeaponType.AXE);
        validWeaponTypes.add(Weapon.WeaponType.HAMMER);
        validWeaponTypes.add(Weapon.WeaponType.SWORD);
        validArmorTypes.add(Armor.ArmorType.MAIL);
        validArmorTypes.add(Armor.ArmorType.PLATE);
    }

    @Override
    public double damage() {
        double damage = 1.0 + (totalAttributes().getStrength())/100.0;
        if(equipment.get(Item.Slot.WEAPON) == (null)) return damage;
        return ((Weapon)equipment.get(Item.Slot.WEAPON)).getWeaponDamage() * damage;
    }

    @Override
    public void levelUp() {
        super.levelUp();
        attributes.addIntelligence(1);
        attributes.addDexterity(2);
        attributes.addStrength(3);
    }

}
