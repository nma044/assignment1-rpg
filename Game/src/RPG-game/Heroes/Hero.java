package Heroes;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Item;
import Items.Weapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public abstract class Hero implements IHero {

    protected String name;
    protected int level = 1;
    protected HeroAttribute attributes;

    public HashMap<Item.Slot, Item> getEquipment() {
        return equipment;
    }

    protected HashMap<Item.Slot, Item> equipment = new HashMap<>();
    protected List<Weapon.WeaponType> validWeaponTypes = new ArrayList<>();
    protected List<Armor.ArmorType> validArmorTypes = new ArrayList<>();

    public Hero(String name) {
        this.name = name;
        putEmptySlots();
    }

    public void putEmptySlots(){
        equipment.put(Item.Slot.WEAPON, null);
        equipment.put(Item.Slot.BODY, null);
        equipment.put(Item.Slot.HEAD, null);
        equipment.put(Item.Slot.LEGS, null);
    }


    @Override
    public void levelUp() {
        level++;
    }

    @Override
    public void equip(Weapon weapon) throws InvalidWeaponException {
        if(level < weapon.getRequiredLevel())
            throw new InvalidWeaponException("Too low level");
        if(!validWeaponTypes.contains(weapon.getWeaponType()))
            throw new InvalidWeaponException("Invalid weapon type");
        equipment.put(Item.Slot.WEAPON, weapon);
    }

    @Override
    public void equip(Armor armor) throws InvalidArmorException {
        if(level < armor.getRequiredLevel())
            throw new InvalidArmorException("Too low level");
        if(!validArmorTypes.contains(armor.getArmorType()))
            throw new InvalidArmorException("Invalid armor type");
        equipment.put(armor.getSlot(), armor);
    }

    @Override
    public abstract double damage();

    @Override
    public HeroAttribute totalAttributes(){
        HeroAttribute total = new HeroAttribute(0,0,0);
        total.addDexterity(attributes.getDexterity());
        total.addIntelligence(attributes.getIntelligence());
        total.addStrength(attributes.getStrength());
        for (Item.Slot armor : equipment.keySet()) {
            if(equipment.get(armor) == (null) || armor.equals(Item.Slot.WEAPON)) continue;
            total.addStrength(((Armor)equipment.get(armor)).getAttributes().getStrength());
            total.addDexterity(((Armor)equipment.get(armor)).getAttributes().getDexterity());
            total.addIntelligence(((Armor)equipment.get(armor)).getAttributes().getIntelligence());
        }
        return total;
    }

    @Override
    public String display(){
        StringBuilder displayText = new StringBuilder();

        displayText.append("Name: "+ name + '\n');
        displayText.append("Class: "+ this.getClass().getSimpleName()+'\n');
        displayText.append("Level: "+level+'\n');
        displayText.append("Total strength: " + totalAttributes().getStrength()+'\n');
        displayText.append("Total dexterity: " + totalAttributes().getDexterity()+'\n');
        displayText.append("Total intelligence: " + totalAttributes().getIntelligence()+'\n');
        displayText.append("Damage: "+damage());

        System.out.println(displayText);
        return displayText.toString();
    }
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

}